#!/bin/bash

set -eE

error() {
  cat <<< "$@" >&2
}

if test ${#BASH_SOURCE[@]} -lt 2; then
  error "Do not call this directly!"
  exit 1
fi

random() {
  shuf -ern 10 {a..z} | tr -d '\n'
}

helper=$(random)

cleanup() {
  podman rm -fi $helper
  podman rm -fi orderly
  podman rm -fi confusion
}

chcon -R -t container_file_t .
PARAMS="--rm -t -v $PWD:$PWD -w $PWD"
IMAGE="quay.io/centos/centos:stream9"

trap "echo 'Caught signal! Exiting..'; cleanup" ERR SIGINT

printf "%s\n" "-- Starting test ${BASH_SOURCE[1]}"

if ! test -x tst_sys_af-unix; then
  printf "%s\n" "-- compiling test"
  gcc -o tst_sys_af-unix af_unix.c -Wall -Werror
fi

if ! test -x tst_af-unix; then
  printf "%s\n" "-- compiling test in container"
  podman run $PARAMS --name $helper $IMAGE bash -c "dnf -y install gcc && \
                                                    gcc -o tst_af-unix  af_unix.c -Wall -Werror"
fi

