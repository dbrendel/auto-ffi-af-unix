#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running shm"
./tst_sys_af-unix >/dev/null &

printf "%s\n" "-- Running shm test in confusion"
podman run $PARAMS --name confusion $IMAGE ./tst_af-unix > /dev/null

cleanup

