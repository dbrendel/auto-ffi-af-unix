#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running af_unix test in orderly"
podman run $PARAMS -d --name orderly $IMAGE ./tst_af-unix > /dev/null

printf "%s\n" "-- Running af_unix test in orderly"
podman exec orderly ./tst_af-unix > /dev/null

cleanup

