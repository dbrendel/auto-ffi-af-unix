# Freedom From Interference (FFI) - SHM access restrictions

This test set should investigate the isolation as well as access possibilities across container boundaries.

## Test cases
 
1. abstract unix domain sockets created on the system are inaccessible in containers
1. abstract unix domain sockets created in one container are inaccessible by the system
1. abstract unix domain sockets created in one container are inaccessible in another container
1. abstract unix domain sockets created in one container are accessible in same container (to verify test functionality)

